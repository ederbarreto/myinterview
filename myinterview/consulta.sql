select count(1) as count, gender from employees group by gender;

select count(1) as count, gender, year(hire_date) as hire_year, year(birth_date) as birth_year from employees group by gender, year(hire_date), year(birth_date);

select avg(s.salary) as Media, min(s.salary) as Min, Max(s.salary) Max, e.gender from salaries s inner join employees e on e.emp_no = s.emp_no group by e.gender;