package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
	
	public static void main(String[] args) {
		List<String> lista = new ArrayList();
		
		Random r = new Random();
		Integer x = r.nextInt(10);
		
		for (int i = 0; i < x; i++) {
			lista.add("teste 1");
			lista.add("teste 2");
			lista.add("teste 3");
			lista.add("teste 4");
			lista.add("teste 5");
			lista.add("teste 6");
			lista.add("teste 7");
		}
		
		int y = (int) lista.stream().distinct().count();
		System.out.println(y);
	}

}
