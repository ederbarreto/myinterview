package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
	
	
	public static void main(String[] args) {
		System.out.println(validaPalindrome("Roma me tem amor"));
	}
	
	public static boolean validaPalindrome(String frase) {
		frase = frase.toLowerCase().replaceAll(" ", "");
		String revertido = new StringBuilder(frase).reverse().toString();
		
		if(frase.equals(revertido)) {
			return true;
		} else {
			return false;
		}
	}

}
