package com.example;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 {
	
	public static class ListaLigada<T>{
		
		private static class Item<T>{
			public Item<T> anterior = null;
			public T valor = null;
			public Item<T> proxima = null;
		}
		
		public Item<T> atual = null;
		
		public void add(T t) {
			Item<T> novo = new Item<T>();
			novo.valor = t;
			novo.proxima = null;
			if(atual == null) {
				novo.anterior = null;
			} else {
				novo.anterior = atual;
				atual.proxima = novo;
			}
			atual = novo;
		}
		
		public void remove(T t) {
			Item<T> percorre = atual;
			while(percorre != null) {
				if(percorre.valor.equals(t)) {
					if(percorre.anterior != null) {
						percorre.anterior.proxima = percorre.proxima;
					}
					if(percorre.proxima != null) {
						percorre.proxima.anterior = percorre.anterior;
					}
					if(atual.equals(percorre)) {
						if(atual.anterior != null) {
							atual = atual.anterior;
						} else {
							atual = null;
						}
					}
					break;
				}
				percorre = percorre.anterior;
			}
			
		}
		
		public String toString() {
			StringBuilder builder = new StringBuilder();
			Item<T> percorre = atual;
			while(percorre != null) {
				builder.insert(0, percorre.valor.toString() + " ");
				percorre = percorre.anterior;
			}
			return builder.toString();
		}
	}
	
	public static void main(String[] args) {
		ListaLigada<String> l = new ListaLigada<String>();
		l.add("Um");
		l.add("Dois");
		l.add("Três");
		l.add("Quatro");
		System.out.println(l);
		l.remove("Dois");
		System.out.println(l);
	}
	
}
